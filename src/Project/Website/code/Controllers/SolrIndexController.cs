﻿using HW.Models;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace HW.Controllers
{
    public class SolrIndexController : Controller
    {
        public ActionResult DoSearch(string searchText)
        {
            var myResults = new SearchResults
            {
                Results = new List<SearchResult>()
            };
            var searchIndex = ContentSearchManager.GetIndex("sitecore_web_index"); // Get the search index
            var searchPredicate = GetSearchPredicate(searchText); // Build the search predicate
            using (var searchContext = searchIndex.CreateSearchContext()) // Get a context of the search index
            {
                //Select * from Sitecore_web_index Where Author="searchText" OR Description = "searchText" OR Title = "searchText"
                var searchResults = searchContext.GetQueryable<SearchModel>().Where(searchPredicate); // Search the index for items which match the predicate

                // This will get all of the results, which is not reccomended
                var fullResults = searchResults.GetResults();
                // This is better and will get paged results - page 1 with 10 results per page
                 //var pagedResults = searchResults.Page(1, 10).GetResults();
                foreach (var hit in fullResults.Hits)
                {
                    Sitecore.Data.Fields.ImageField ifd = hit.Document.GetItem().Fields["Image"];
                    Sitecore.Data.Items.MediaItem img = new Sitecore.Data.Items.MediaItem(ifd.MediaItem); 
                    string imgsrc = Sitecore.StringUtil.EnsurePrefix('/',Sitecore.Resources.Media.MediaManager.GetMediaUrl(img));
                    myResults.Results.Add(new SearchResult
                    {
                        Title = hit.Document.Title,
                        Short = hit.Document.Short,
                        Date = hit.Document.Date.ToShortDateString(),
                        Url = hit.Document.GetItem().Paths.Path,
                        Image = imgsrc
                    });
                }
                return new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = myResults
                };
            }
        }
        /// <summary>
        /// Search logic
        /// </summary>
        /// <param name="searchText">Search term</param>
        /// <returns>Search predicate object</returns>
        public static Expression<Func<SearchModel, bool>> GetSearchPredicate(string searchText)
            {
                var predicate = PredicateBuilder.False<SearchModel>(); // Items which meet the predicate
                predicate = predicate.Or(x => x.Short.Contains(searchText)); // .Boost(2.0f);
                predicate = predicate.Or(x => x.Title.Contains(searchText)); // .Boost(2.0f);
                predicate = predicate.And(x => !String.IsNullOrEmpty(x.Short)); // .Boost(2.0f);
                                                                             //Where Author="searchText" OR Description="searchText" OR Title="searchText"
                return predicate;
            }
    }
}
