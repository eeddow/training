﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using System;
using System.Collections.Generic;
namespace HW.Models
{
    public class SearchModel : SearchResultItem
    {
        [IndexField("title_t")]
        public virtual string Title { get; set; }
        [IndexField("short_t")]
        public virtual string Short { get; set; }
        [IndexField("date_t")]
        public virtual DateTime Date { get; set; }
        [IndexField("image_t")]
        public virtual string Image { get; set; }


    }
    public class SearchResult
    {
        public string Title { get; set; }
        public string Short { get; set; }
        public string Url { get; set; }
        public string Date { get; set; }
        public virtual string Image { get; set; }
    }
    /// <summary>
    /// Custom search result model for binding to front end
    /// </summary>
    public class SearchResults
    {
        public List<SearchResult> Results { get; set; }
    }
}
