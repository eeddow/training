﻿using Sitecore.Pipelines;
using System.Web.Mvc;
using System.Web.Routing;

namespace HW
{
    public class RegisterSolrCustomRoute
	{
		public virtual void Process(PipelineArgs args)
		{
			Register();
		}

		public static void Register()
		{
			RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			RouteTable.Routes.MapRoute("SearchRoute", "api/gatewayAPI/{controller}/{action}");
		}
	}
}